package tp5;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Convertisseur extends JFrame implements ActionListener {
	double ht;
	double ttc;
	double tva;
	Container panneau;
	JButton convertir=new JButton("Convertir");
	JLabel labeltva=new JLabel("TVA:");
	JLabel labelht=new JLabel("Prix HT");
	JLabel labelttc=new JLabel("Prix TTC");
	JPanel p=new JPanel();
	JPanel p1=new JPanel();
	
	JTextField textetva= new JTextField();
	JTextField texteht= new JTextField();
	JTextField textettc= new JTextField();

	public Convertisseur(){
		setSize(350,125);
		setLocation(30,150);
		setTitle("Convertisseur");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panneau=getContentPane();
		p.setLayout(new GridLayout(1,2));
		p1.setLayout(new GridLayout(1,4));
		p.add(labeltva);
		p.add(textetva);
		p1.add(labelht);
		p1.add(texteht);
		p1.add(labelttc);
		p1.add(textettc);
		
		panneau.add(p,BorderLayout.NORTH);
		panneau.add(p1,BorderLayout.CENTER);
		panneau.add(convertir,BorderLayout.SOUTH);
		convertir.addActionListener(this);
		setVisible(true);
	}
	public void versTTC (double ht){
		ttc=tva*ht+ht;
		textettc.setText(String.valueOf(ttc));
		
	}
public void actionPerformed(ActionEvent e){
	ht=Double.parseDouble(texteht.getText());
	tva=Double.parseDouble(textetva.getText());
		versTTC(ht);
	}

	public static void main(String[] args) {
		Convertisseur c=new Convertisseur();

	}
	
	


}
