package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConvertisseurEuroDollar  extends JFrame implements ActionListener {
	
	double prixe;
	double prixd;
	double taux;
	int i=1;
	
	Container panneau;
	JButton convertir=new JButton("Convertir");
	JButton quitter=new JButton("Quitter");
	JButton sens=new JButton("==>");
	JLabel label1=new JLabel("�");
	JLabel label2=new JLabel("$");
	JLabel label3=new JLabel("Taux 1�=");
	JLabel label4=new JLabel("$");
	JPanel p=new JPanel();
	JPanel p1=new JPanel();
	JPanel p2=new JPanel();
	
	JTextField texte1= new JTextField();
	JTextField texte2= new JTextField();
	JTextField texte3= new JTextField();

	public ConvertisseurEuroDollar(){
		setSize(350,125);
		setLocation(30,150);
		setTitle("ConvertisseurEuroDollar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panneau=getContentPane();
		p.setLayout(new GridLayout(1,5));
		p1.setLayout(new GridLayout(1,3));
		p2.setLayout(new GridLayout(1,2));
		p.add(texte1);
		p.add(label1);
		p.add(sens);
		p.add(texte2);
		p.add(label2);
		p1.add(label3);
		p1.add(texte3);
		p1.add(label4);
		p2.add(convertir);
		p2.add(quitter);
		panneau.add(p,BorderLayout.NORTH);
		panneau.add(p1,BorderLayout.CENTER);
		panneau.add(p2,BorderLayout.SOUTH);
		convertir.addActionListener(this);
		sens.addActionListener(this);
		quitter.addActionListener(this);
		
		setVisible(true);
	}
	void Conversion(){
		if(sens.getText()=="==>"){	
			taux=Double.parseDouble(texte3.getText());
			prixe=Double.parseDouble(texte1.getText());
			prixd=taux*prixe;
			texte2.setText(String.valueOf(prixd));
		}
		
		else{
			taux=Double.parseDouble(texte3.getText());
			prixd=Double.parseDouble(texte2.getText());
			prixe=taux*prixd;
			texte1.setText(String.valueOf(prixe));
		}
	
	}
	
	void changementDeSens(){
		if(sens.getText()=="==>"){
	
			label3.setText("Taux 1�=");
			label4.setText("$");
			
		}
		else{
			
			label3.setText("Taux 1$=");
			label4.setText("�");
		}
		texte1.setText("");
		texte2.setText("");
		texte3.setText("");

	}
		

public void actionPerformed(ActionEvent e){
	
if(e.getSource()==convertir){
	Conversion();
}
if(e.getSource()==quitter){
	this.dispose();
}
if(e.getSource()==sens){
	if(i%2!=0){
		sens.setText("<==");
	
	}
	else{
		sens.setText("==>");
		
	}
	changementDeSens();
	i++;
	
	
}
	}

	public static void main(String[] args) {
		ConvertisseurEuroDollar c=new ConvertisseurEuroDollar();

	}
	
}
