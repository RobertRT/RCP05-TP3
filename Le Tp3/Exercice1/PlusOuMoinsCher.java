package tp5;
//EXERCICE 5
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PlusOuMoinsCher extends JFrame implements ActionListener{
	
	Container panneau;
	int nbrtour=0;
	int reponse;
	int nbrdeviner=0+(int)(Math.random()*((100-0)+1));
	JButton verifie=new JButton("Verifie!");
	JLabel label1=new JLabel("Votre Proposition:");
	JLabel label2=new JLabel("La r�ponse");
	JTextField texte1= new JTextField();
	
	public PlusOuMoinsCher(){
		setSize(350,150);
		setLocation(30,150);
		setTitle("Plus Ou Moins Cher?");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panneau=getContentPane();
		panneau.add(label1);
		panneau.add(texte1);
		panneau.add(verifie);
		panneau.add(label2);
		panneau.setLayout(new GridLayout(2,2));
		verifie.addActionListener(this);
		setVisible(true);
	}
	
	void ComparerNbre(){
		
		if(nbrtour<7){
		reponse=Integer.parseInt(texte1.getText());
		if(reponse<nbrdeviner){
			label2.setText("Plus Cher!");
			nbrtour++;
		}
		else if(reponse>nbrdeviner){
			label2.setText("Moins Cher!");
			nbrtour++;
		}
		else{
			label2.setText("Bonne r�ponse!");
			Init();
	
		}
		}
		else{
			label2.setText("Vous Avez Perdu!");
			Init();
			nbrtour=0;
		}

		
	}
	
	void Init(){
		nbrdeviner=0+(int)(Math.random()*((100-0)+1));
		texte1.setText("Entrer un nouveau nombre");
	}
	
	public void actionPerformed(ActionEvent e){
		
		ComparerNbre();
	}
	public static void main(String[] args) {
		PlusOuMoinsCher p=new PlusOuMoinsCher();

	}
	

}
